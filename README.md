# Hello, world.

```mermaid
graph TD
  A[git] -- behind the scenes --> B(plubming)
  A -- user-facing, user-friendly --> C(porcelain)
```

## DIY Git Porcelain
Want to create your own Git porcelain commands? 

I have found `git hist` a nice complement to `git log`. Add to `~/.gitconfig`.

[alias]
  hist = log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short

## Useful Documentation | GitLab
  - [Signing commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)


## Useful Git Resources
These are sites that present `git` information in a way that I personally find helpful. Your mileage may vary. 

  * [githowto.com](https://githowto.com) - basic

## One Liners
Get all PIDs in a single line:
`pgrep -f nginx  | tr '\n' ',' | sed 's/,$//'`

Build on the oneliner above: run strace against all matching processes with the [strace-parser](https://gitlab.com/gitlab-com/support/toolbox/strace-parser) preferred format



`strace -fttTyyy -s 1024 -p $(pgrep -f puma  | tr '\n' ',' | sed 's/,$//') --output=nginx-strace`

## GitLab
### Backups
#### Application
Create an application backup with `gitlab-backup create`. These backups will be stored in `/var/opt/gitlab/backups/`. 
#### Configuration
Preserve the secrets and configuration data with `gitlab-ctl backup-etc`. These backups will be stored in `/etc/gitlab/config_backup`.

## Restores
  * Digital Ocean: `13.0.6-ee.0`
  * Docker: `13.1.1-ce.0`

The documentation for the backup and restore process describes [making sure that the GitLab versions match](https://docs.gitlab.com/omnibus/update/README.html#downgrading) between the GitLab instance that the backup was performed on and the GitLab instance that the restore is being applied to. 

Pro tip on restoring: the `gitlab-ctl restore` command allows you to specify the `BACKUP` you want to use. This is the filename of the restore without the trailing `_gitlab_backup.tar`. 

### Questions
Must `gitlab-ctl reconfigure` run successfully?

